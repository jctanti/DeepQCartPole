import gym
import numpy as np
import tensorflow as tf
from RLDeepAgents import RLDiscreteDQL
import random
from collections import deque
from matplotlib import pyplot as plt

env_name = "CartPole-v0"
env = gym.make(env_name)

seed = 42
env.seed(seed)
np.random.seed(seed)

# Simulation parameters
episode_count = 100000
max_steps = 10000
expMaxLen = 2000

# Batch size for sampling from experience replay
expBatchSize = 32

# Learning rate of Adam optimization; static for now
learningRate = 1e-3

# Ratio for transfering Q-network to Q-Target-network; not currently being used
tau = 1e-1

# epsilon max and min, and rate of decay per step
epsilonMax = 1
epsilonMin = 0.001
epsilonDecay = .999

# Discount factor for temporal difference
gamma = .99

# Number of steps between Q-Target network update
numStepsToTargetUpdate = 100

# Network layer sizes, can be more than 2 if you want, but CartPole is a fairly simple environment
networkLayerSizes = [16, 16]

# Number of episodes to average to get performance measure
# OpenAI Gym website says that solving CartPole means the agent can hit an average reward of 195.0 or more over the last 100 episodes
maxPrevRewardLen = 100

num_actions = env.action_space.n
input_state_shape = env.observation_space.shape

# Creat the Agent
theAgent = RLDiscreteDQL(input_state_shape[0], num_actions, networkLayerSizes, epsilonMax, epsilonMin, epsilonDecay, gamma, learningRate, tau, expMaxLen, expBatchSize, numStepsToTargetUpdate)

# Deque to store previous 100 rewards
prevRewards = deque(maxlen=maxPrevRewardLen)

# Array to store all means of past 100 rewards
prevMeans = []

# Go go go!
for curEp in range(episode_count):
    
    state = env.reset()
    allReward = 0
    
    for curStep in range(max_steps):
        
        # Comment out this line if you want to see the performance measures printed, but not the cartpole GUI rendered; saves time for simulation b/c rendering takes time
        env.render()
        
        # Take an action
        nextAction = theAgent.takeAction(state.reshape(1,-1))[0]
        next_state, reward, is_terminal, info = env.step(nextAction)

        # Train network with new transition
        theAgent.processReward(reward, state, nextAction, next_state, is_terminal)  
        
        # Update state and increment total reward
        state = next_state
        allReward += reward
        
        if is_terminal == True:
            break
        
    prevRewards.append(allReward)
    print("Ep %d : %d | Epsilon: %f" % (curEp, allReward, theAgent.epsilon))
    
    if len(prevRewards) == maxPrevRewardLen:
        print("Average Reward (last %d): %f" % (maxPrevRewardLen, np.mean(prevRewards)))
        prevMeans.append(np.mean(prevRewards))
        
    if np.mean(prevRewards) > 195.0:
        plt.plot(range(len(prevMeans)), prevMeans)
        plt.title("Average Rewards of Past 100 Trials")
        plt.xlabel("Episode")
        plt.ylabel("Average Reward")
        plt.show()

