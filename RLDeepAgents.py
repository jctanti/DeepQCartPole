import numpy as np
from random import randint
import random
from RLUtils import ExperienceReplay
import tensorflow as tf
import keras

# Agent with Q function approximation for discrete actions
# In this case, we need only 1 network, the Q network, that we will update through TD (not including "target" network for smooth updates)
# Agent really only needs 3 functions:
# takeAction(state)
# processReward(r,s,a,s')
# getQFunction()
class RLDiscreteDQL:
    
    def __init__(self, stateDim, numActions, nHidden, epsilonMax, epsilonMin, epsilonDecay, gamma, learningRate, tau, expMaxLen, expBatchSize, numStepsToTargetUpdate):
        
        # in the case of a Q network, we need to know the dimensionality of each potential s to initialize the input to the network
        self.stateDim = stateDim
        
        # The Q network will have number of outputs = numActions, and the output will be the value of executing that action given the state
        self.numActions = numActions
        
        # Epsilon for exploration randomness
        self.epsilon = epsilonMax
        self.epsilonMin = epsilonMin
        self.epsilonDecay = epsilonDecay
        self.gamma = gamma
        self.learningRate = learningRate
        self.tau = tau
        
        # Architecture of network
        self.nHidden = nHidden
        
        #Initialize experience replay
        self.expReplay = ExperienceReplay(expMaxLen)
        self.expMaxLen = expMaxLen
        self.expBatchSize = expBatchSize
        
        # Denotes number of steps before target network gets copied, assume we do straight-up copying instead of ratio-based copy
        self.numStepsToTargetUpdate = numStepsToTargetUpdate
        
        # Create networks
        self.initializeNetworks()
        self.initializeTensorflow()
        
        self.numSteps = 0
    
    # Intialize all the tensorflow stuff
    def initializeTensorflow(self):
        self.sess = tf.Session()
        self.globalInit.run(session=self.sess)
        self.initializeNetworkCopies.run(session=self.sess)
    
    # Actually create the network in the graph
    def createQNetwork(self, name):
        nInputs = self.stateDim
        nHidden = self.nHidden
        
        # We'll do variance scaled init and relu activations for now
        initializer = tf.contrib.layers.variance_scaling_initializer()
        activation = tf.nn.relu
        
        # X is the input, which is exactly the state 
        # y is the output, which is exactly the number of actions
        X = tf.placeholder(tf.float32, shape=(None, nInputs), name="X")
        
        # Construct the network using specifications
        with tf.variable_scope(name) as scope:
            
            # Create first layer, and then loop over the rest
            h = tf.layers.dense(X, nHidden[0], name="h0", activation=activation)
            
            for i in range(1, len(nHidden)):
                layerName = "h-" + str(i)
                h = tf.layers.dense(h, nHidden[i], name=layerName, activation=activation)
                
            # For now, we accommodate only a linear final layer, as that lends well to the reinforcement learning
            # problem with discrete actions
            wOut = tf.Variable(tf.truncated_normal([nHidden[len(nHidden)-1], self.numActions], mean=0, stddev=1))
            bOut = tf.Variable(tf.zeros([self.numActions]))
            outputs = tf.matmul(h, wOut) + bOut
            
        # Get variables to train from the dnn   
        trainingVars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=name)
        
        # Names that serve as keys in this map will be everything after the actual name of the network itself, so we can compare multiple networks later
        # So if the network name is "qNets/qActor", and a variable is of name "qNets/qActor/h1/kernel:0", then the key in the map is just "/h1/kernel:0"
        trainingVarsByName = {var.name[len(scope.name):]: var for var in trainingVars}
        
        return X, outputs, trainingVarsByName, trainingVars
    
    # Construction phase for networks for Q learning   
    def initializeNetworks(self):
        
        # These two networks should have the exact same shape; it's just the target only updates every so often to maintain
        # stability of the learning algorithm
        self.QInput, self.Q, self.QVarsByName, self.QVars = self.createQNetwork("qNets/qFunction")
        self.QTargetInput, self.QTarget, self.QTargetVarsByName, self.QTargetVars = self.createQNetwork("qNets/qTarget")
        
        # Create op that copies the variables of the same name from the true actor network to the target network using gradual ratio
        copyOps = [targetVar.assign(targetVar * (1 - self.tau) + self.tau * self.QVarsByName[varName]) for varName, targetVar in self.QTargetVarsByName.items()]     
        self.opCopyToTarget = tf.group(*copyOps)
        
        # Create op that straight up copies params from the Q network to the Q-Target network
        copyOps2 = [targetVar.assign(self.QVarsByName[varName]) for varName, targetVar in self.QTargetVarsByName.items()]
        self.initializeNetworkCopies = tf.group(*copyOps2)
        
        # Create loss computation
        with tf.name_scope("loss"):
            
            # Placeholder for action from replay memory, so we can get its q value
            self.X_action = tf.placeholder(tf.int32, shape=[None])
            
            # This first makes a 1-hot vector in the position of the action that was taken, and multiplies it by the output
            # of the network, and takes the sum; the overall effect is just getting the output that corresponds to the taken action
            qValue = tf.reduce_sum(self.Q * tf.one_hot(self.X_action, self.numActions), axis=1, keep_dims=True)

            # y will have the value of reward + gamma * max(\hat{Q})
            self.yTarget = tf.placeholder(tf.float32, shape=[None, 1])
            loss = tf.reduce_mean(tf.square(self.yTarget - qValue))
        
        # Create optimizer and training op with loss computation   
        with tf.name_scope("train"):
            globalStep = tf.Variable(0, trainable=False, name='global_step')
            
            # Using Adam for now
            optimizer = tf.train.AdamOptimizer(self.learningRate)
            
            grads = tf.gradients(loss, self.QVars)
            
            # This was experimental, it clipped the gradsto be between -1 and 1; doesn't seem to do much
            #grads = [tf.clip_by_value(grad, -1, 1) for grad in grads]

            # Create the training op using the computed gradient
            self.opTrainQNetwork = optimizer.apply_gradients(zip(grads, self.QVars), global_step=globalStep)
        
        # Intialize all variables   
        self.globalInit = tf.global_variables_initializer()
        
    # Returns action based on argmax or softmax + sampling given the state
    def takeAction(self, state):
        
        # Check epsilon-greedy probability
        doRandom = random.uniform(0, 1)
        
        # If epsilon hits, do random action; otherwise, pick best action in Q
        if doRandom < self.epsilon:
            nextAction = [randint(0, self.numActions-1)]
        else:
            nextAction = np.argmax(self.Q.eval(session=self.sess, feed_dict={self.QInput: state}), axis=1)
        
        return nextAction
    
    # Append new 4-tuple to experience replay, sample batch, and run training op
    def processReward(self, r, s1, a, s2, terminal):
        
        # Add current transition to experience replay
        self.expReplay.addExperience(s1, a, r, s2, terminal)
        
        # sample minibatch; will return a matrix where every row is one experience, in SARST order
        expStates, expActions, expRewards, expStatesNext, expTerminals = self.expReplay.sampleExperiences(int(self.expBatchSize))
        
        # Train the network
        # Get the Q values from the "end" states for each transition
        qHat = self.QTarget.eval(session=self.sess, feed_dict={self.QTargetInput: expStatesNext})
        
        # compute the target value; be CAREFUL with the elementwise stuff with np.multiply...
        yVal = expRewards + self.gamma * np.multiply(np.abs(expTerminals.reshape(-1,1) - 1), np.max(qHat, axis=1).reshape(-1,1))
        
        # Run the training op given the taken actions, the initial states, and the target values
        self.opTrainQNetwork.run(session=self.sess, feed_dict={self.X_action : expActions, self.QInput : expStates, self.yTarget : yVal})
        
        # Epsilon decay
        if self.epsilon > self.epsilonMin:
            self.epsilon = self.epsilon * self.epsilonDecay
            
        if self.numSteps % self.numStepsToTargetUpdate == 0:
            self.initializeNetworkCopies.run(session=self.sess)
        
        self.numSteps += 1
            
            