#Utility functions/classes for Reinforcement Learning stuff

import numpy as np
from collections import deque

# helper class for experience replay deque that also allows sampling of random batch
class ExperienceReplay:
    
    def __init__(self, maxlen):
        self.experienceList = deque(maxlen=maxlen)
    
    def addExperience(self, s1, a, r, s2, terminal):
        self.experienceList.append([s1,a,r,s2, terminal])
     
    #sample numExperiences from self.experienceList and return an array of them   
    def sampleExperiences(self, numExperiences):
        
        if numExperiences > len(self.experienceList):
            numExperiences = len(self.experienceList)
       
        expIndices = np.random.choice(len(self.experienceList), numExperiences, replace=False)
        #print("Exp list: ", self.experienceList)
        
        return np.array([self.experienceList[i][0] for i in expIndices]), np.array([self.experienceList[i][1] for i in expIndices]), np.array([self.experienceList[i][2] for i in expIndices]).reshape(-1,1), np.array([self.experienceList[i][3] for i in expIndices]), np.array([self.experienceList[i][4] for i in expIndices]) * 1
    